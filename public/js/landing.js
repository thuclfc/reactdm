$(document).ready(function () {
    open_popup('.video','.popup-video');

    $('.charcter__select li').on('click',function () {
        $('.charcter__select li').removeClass('active');
        $(this).addClass('active');
        let sect_img = $(this).data('img');
        let sect_video = $(this).data('video');
        let sect_class = $(this).data('class');
        let html_img = `<div class="charcter__img fadeIn animated ${sect_class}"><img src="${sect_img}" alt=""></div>`;
        let html_trailer = `<div class="charcter__video"><iframe src="${sect_video}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>`;
        $('.charcter__content').html('').append(html_img, html_trailer);
    });

    $('.popup-vq').modal('show');


    $(function() {
        $('#dg-container').gallery({
            autoplay	:	false,
        });
    });
});